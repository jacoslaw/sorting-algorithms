var SystemDrawer = function(system, canvas) {
	this.system = system;
	this.canvas = canvas;
	this.colors = ['#31082b', '#57306f', '#646583', '#de8800', '#6b9c7d', '#a7ca50',
		           '#e2f528', '#c91515', '#7b0429', '#02ff17', '#0239ff', '#8F6F05', 
		           '#EE0202', '#FF7C00', '#DE28F6'];

	this.draw = function(processes) {
		var that = this;
		var acc = 0;
		var processes = this.system.processes;
		while(this.canvas.firstChild) {
			this.canvas.removeChild(this.canvas.firstChild);
		}
		processes.forEach(function(process, index) {
			var rectangle = that.createSvgElement('rect', {'width': '200',
													  	   'height': process.length*30,
													       'y': acc,
													       'fill': that.colors[process.id % that.colors.length]})
			that.canvas.appendChild(rectangle);

			var text = that.createSvgElement('text', {'x': '10',
									 			 	  'y': '10',
									  			 	  'font-size': '120'});
			var textNode = document.createTextNode(process.name);
			text.appendChild(textNode);
			rectangle.appendChild(text);

			acc = acc + process.length*30 + 1;
		});
	};
    this.createSvgElement = function(type, attributes) {
        var element = document.createElementNS("http://www.w3.org/2000/svg", type);
		for(attribute in attributes) {
		    element.setAttribute(attribute, attributes[attribute]);
		}
		return element;
    }
};