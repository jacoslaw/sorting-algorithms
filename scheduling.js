var Process = function(name, length, id, born) {
	this.name = name;
	this.length = length;
	this.initialLength = length;
	this.id = id;
	this.born = born;  // data "urodzenia" wyrazona wartoscia systemowego timera
	this.finished = undefined;
}

var System = function(schedulerType, onProcessesFinish) {	
	// schedulerType moze byc jednym z:
	// 'FCFS' 'SJF' 'RR' 'SJF+A' (SJF z postarzaniem "aging")
	this.processes = [];
	this.finished = [];
	this.processIdCounter = 1;
	this.schedulerType = schedulerType;
	this.timer = 0;
	this.onProcessesFinish = onProcessesFinish;
	this.currentProcess = undefined;

	this.createProcess = function(name, length) {
		process = new Process(name, length, this.processIdCounter, this.timer);
		this.processIdCounter = this.processIdCounter + 1;
		this.processes.push(process);
		this.schedule();
		return process;
	};

	this.finishProcess = function(process) {
		process.finished = this.timer;
		this.finished.push(process);
	}

	this.setScheduler = function(schedulerType) {
		this.schedulerType = schedulerType;
	};

	this.schedule = function() {
		var process;
		if(this.processes.length > 0) {
			var p1 = this.processes[0];
			switch (this.schedulerType) {
				case 'RR': 	if(p1.length <= 0) {
								this.processes.splice(0, 1);
							} else {
								if(this.currentProcess) {
								    this.processes.splice(0, 1);
				      				this.processes.push(this.currentProcess);
				      			}				
				      		}
				     	  	break;
				case 'SJF': if(p1.length <= 0) {
								this.processes.splice(0, 1);
							}
							this.processes.sort(function(p1, p2) {
					  			return (p1.length - p2.length);
					   		});
					   		break;
				case 'SJF+A': 	var that = this;
				             	if(p1.length <= 0) {
									this.processes.splice(0, 1);
								}
							   	this.processes.sort(function(p1, p2) {
				             		var p1Age = that.timer - p1.born;
				             		var p2Age = that.timer - p2.born;
				             		return (p1.length - p1Age) - (p2.length - p2Age);
				             	})
				             	break; 
				// FCFS
			    default: 	if(p1.length <= 0) {
								this.processes.splice(0, 1);
							}							
			    			this.processes.sort(function(p1, p2) {
			    				return p1.born - p2.born;
			    			})
			}
			if(this.processes.length === 0 && this.onProcessesFinish !== undefined) {
				this.onProcessesFinish(this);
			}
		} 
	};

	this.process = function(){
		// zakończ przetwarzanie bieżącego processu
		if(this.currentProcess !== undefined) {
			this.currentProcess.length = this.currentProcess.length - 1;
			if(this.currentProcess.length === 0) {
				this.finishProcess(this.currentProcess);
			}
		}
		// zmodyfikuj kolejkę procesów zgodnie z bieżącym algorytmem
		this.schedule();
		// jeśli kolejka procesów jest nie pusta wybierz 
		// proces do przetwarzania
		if(this.processes.length !== 0) {
			this.currentProcess = this.processes[0];
		} else {
			this.currentProcess = undefined;
		}

		this.timer = this.timer + 1;
		return this.processes;
	};
}
